all: bot

bot: bot.rs
	rustc -o bot bot.rs

run: all
	./bot

clean:
	rm -f bot
