#!/usr/bin/env python3

import os
import platform
import subprocess
from telegram import Update
from telegram.ext import Updater, CommandHandler, CallbackContext, MessageHandler, Filters
import markovify

token = ''

# wrapper to generate a sentence
def generate(tries, short=False, meta=False):
    with open("store.txt" if not meta else "meta-store.txt") as f:
        model = markovify.NewlineText(f.read())
        s = model.make_sentence(tries=tries) if not short else model.make_short_sentence(120, tries=tries)
        if s:
            with open("meta-store.txt", "a") as ms:
                ms.write(f"{s}\n")
        return s

# say hello :)
def hello(update: Update, context: CallbackContext) -> None:
    update.message.reply_text(f'Hello {update.effective_user.first_name} :)')

# generate a sentence
def books(update: Update, context: CallbackContext) -> None:
    s = generate(100)
    if not s:
        update.message.reply_text("Could not make sentence. Is the model big enough?")
    else:
        print(f"[NEW BOOK] {s}")
        update.message.reply_text(s)

# try to generate a sentence over and over
def trybooks(update, context):
    s = generate(1000)
    if not s:
        update.message.reply_text("Though I tried 1000 times, I am still too stupid to make a sentence.")
    else:
        print(f"[NEW TRIED BOOK] {s}")
        update.message.reply_text(s)

# make a short sentence
def shortbooks(update, context):
    s = generate(1000, short=True)
    if not s:
        update.message.reply_text("Could not make a short sentence. Is the model big enough?")
    else:
        print(f"[NEW SHORT BOOK] {s}")
        update.message.reply_text(s)

# make a meta sentence
def metabooks(update, context):
    s = generate(1000, short=False, meta=True)
    if not s:
        update.message.reply_text("Could not make meta sentence. Is the model big enough?")
    else:
        print(f"[NEW META BOOK] {s}")
        update.message.reply_text(s)

def date(update, context): update.message.reply_text("uwu")

# get stats
def stat(update, context):
    storel = -1
    metal = -1
    with open("store.txt","r") as f:
        storel = len(f.readlines())
    with open("meta-store.txt", "r") as f:
        metal = len(f.readlines())

    update.message.reply_text(f"""<pre>
store.txt length:   {storel}
meta.txt length:    {metal}
uptime:             {subprocess.run(['uptime'], stdout=subprocess.PIPE).stdout.decode('utf-8')}
sysinfo:            
-    os name:          {os.name}
-    platform system:  {platform.system()}
-    platform version: {platform.release()}
source              https://gitlab.com/monarrk/crackerbooks
</pre>""", parse_mode="html")

def faggot(update, context):
    i = 0
    with open("store.txt", "r") as f:
        for line in f:
            for w in line.split():
                if "faggot" in w:
                    i+=1
    update.message.reply_text(f"{i} instances of 'faggot' in store.")

# run each time a message is sent
# does not run for commands
def msg_callback(update, context):
    # if message is text (text != None)
    if update.message.text:
        print(f"[STORING MESSAGE][{update.effective_user.first_name}] {update.message.text}")
        with open("store.txt", "a") as f:
            text = update.message.text.replace("\n", " ")
            f.write(f"{text}\n")
    else:
        print("[MULTIMEDIA MESSAGE] UNABLE TO STORE MESSAGE AS TEXT")

# get the token from token.txt
with open("token.txt") as f:
    token = f.read().strip()

print(f"Logging in as {token}")
updater = Updater(token)

# command handlers
updater.dispatcher.add_handler(CommandHandler('hello', hello))
updater.dispatcher.add_handler(CommandHandler('books', books))
updater.dispatcher.add_handler(CommandHandler('trybooks', trybooks))
updater.dispatcher.add_handler(CommandHandler('shortbooks', shortbooks))
updater.dispatcher.add_handler(CommandHandler('metabooks', metabooks))
updater.dispatcher.add_handler(CommandHandler('stat', stat))
updater.dispatcher.add_handler(CommandHandler('date', date))
updater.dispatcher.add_handler(CommandHandler('faggot', faggot))
# text callback
updater.dispatcher.add_handler(MessageHandler(Filters.all, msg_callback))

updater.start_polling()
updater.idle()
