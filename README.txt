CRACKERBOOKS
	Another fucking markov bot.

SETUP
	% pip3.8 install markovify python-telegram-bot
	% echo 'Hello there!' > store.txt
	% echo '<YOUR TELEGRAM BOT TOKEN>' > token.txt
	% python3.8 bot.py # or, ./bot.py

COMMANDS
	/hello        receive a friendly greeting
	/books        generate a sentence
	/trybooks     generate a sentence, but try a bunch of times
	/shortbooks   generate a sentence below 120 characters

LICENSE
	This code is dedicated to the public domain under the terms of the UNLICENSE
	See the UNLICENSE file for details, or visit https://unlicense.org
